class Comment < ActiveRecord::Base
  validates :name, presence:true ,length: {minimum: 3, maximum: 50 }
  validates :body, presence:true ,length: {minimum: 3}
  belongs_to :post
end
