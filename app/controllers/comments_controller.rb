class CommentsController < ApplicationController

  def create
    @post=Post.find(params[:post_id])
    @comment=@post.comments.create(params[:comment].permit(:name, :body))
    redirect_to post_path(@post, errors: @comment.errors.messages)
  end

  def edit
    @post=Post.find(params[:post_id])
    @comment=Comment.find(params[:id])
  end

  def update
    @post=Post.find(params[:post_id])
    @comment=@post.comments.find(params[:id])
    if @comment.update(params[:comment].permit(:name, :body))
      redirect_to @comment.post if @comment.save
    else
      redirect_to :back
    end
  end

  def destroy
    @post=Post.find(params[:post_id])
    @comment=@post.comments.find(params[:id])
    @comment.destroy
    redirect_to :back
  end

end
